package cmd

import (
	_ "embed"
	"github.com/spf13/cobra"
)

var licenseCmd = &cobra.Command{
	Use: "license",
	Short: `

--------------------- License -----------------------
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 
												 
   go-jira-cli									 
   Copyright (C) 2020  Paulo Alexandre Canilho	 
													 
You may copy, distribute and modify the software as
long as you track changes/dates in source files.   
Any modifications to or software including (via 	 
compiler) GPL-licensed code must also be made 	 
available under the GPL along with build & install 
instructions.										 
-----------------------------------------------------`,
	Aliases: []string{"l"},
}
