package formatters

import (
	"fmt"
	"gitlab.com/pcanilho/go-jira"
	"gitlab.com/pcanilho/go-jira-cli/cmd/interactive"
	"os"
)

type interactiveFormatter struct {
	menu interactive.Menu
}

func NewInteractiveFormatter() *interactiveFormatter {
	return &interactiveFormatter{}
}
func (i *interactiveFormatter) Serialise(v any) string {
	// @TODO - Convoluted block >
	var m interactive.Menu
	var issues []jira.Issue
	var u jira.User

	switch vs := v.(type) {
	case []jira.Issue:
		issues = vs
	case jira.Issue:
		issues = append(issues, vs)
	case *jira.Issue:
		issues = append(issues, *vs)
	case jira.User:
		u = vs
	case *jira.User:
		u = *vs
	default:
		_, _ = fmt.Fprintln(os.Stderr, fmt.Sprintf("interactive formatter: the supplied type [%T] is not supported!", v))
		return ""
	}

	if len(issues) > 0 {
		m = interactive.NewIssueListMenu(issues, i.menu)
	} else {
		m = interactive.NewUserMenu(u, i.menu)
	}
	// @TODO - Convoluted block />
	_, selected, _ := m.Render()
	return selected
}
