package formatters

import (
	"gopkg.in/yaml.v3"
)

type yamlFormatter struct {}

func NewYAMLFormatter() *yamlFormatter {
	return &yamlFormatter{}
}

func (jf *yamlFormatter) Serialise(v any) string {
	b, err := yaml.Marshal(v)
	if err != nil {
		return ""}
	return string(b)
}
