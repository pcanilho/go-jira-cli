package cmd

import (
	"github.com/spf13/cobra"
)

var operatorCmdMap = map[string][]*cobra.Command{
	"~": {&cobra.Command{
		Use:     "summary",
		Short:   "Searches for a Jira ticket based on the supplied summary content (exit status 1 if no issue was found)",
		Example: `jira-cli issue search summary -p <project_name> -t <issue_type> -s <issue_status> -e <custom_field>=<value> "<summary_content>"`,
	}},
	"=": {
		&cobra.Command{
			Use:     "assignee",
			Short:   "Searches for a Jira ticket that were assignee to the provided identity (exit status 1 if no issue was found)",
			Example: `jira-cli issue search assignee -p <project_name> -m <issue_summary> -t <issue_type> -s <issue_status> -e <custom_field>=<value> "<summary_content>"`,
		},
		&cobra.Command{
			Use:     "project",
			Short:   "Searches for a Jira ticket that belong to the provided project (exit status 1 if no issue was found)",
			Example: `jira-cli issue search assignee -a <assignee> -t <issue_type> -m <issue_summary> -s <issue_status> -e <custom_field>=<value> "<summary_content>"`,
		},
		&cobra.Command{
			Use:     "type",
			Short:   "Searches for a Jira ticket based on the type (exit status 1 if no issue was found)",
			Example: `jira-cli issue search summary -p <project_name> -m <issue_summary> -s <issue_status> -e <custom_field>=<value> "<summary_content>"`,
		},
		&cobra.Command{
			Use:     "status",
			Short:   "Searches for a Jira ticket based issue status (exit status 1 if no issue was found)",
			Example: `jira-cli issue search summary -p <project_name> -t <issue_type> -m <issue_summary> -e <custom_field>=<value> "<summary_content>"`,
		},
	},
}
