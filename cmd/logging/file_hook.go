package logging

import (
	logger "github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
)

type ApplicationHookConfig struct {
	Filename   string
	MaxSize    int
	MaxBackups int
	MaxAge     int
	Level      logger.Level
	Formatter  logger.Formatter
}

type ApplicationHook struct {
	cfg       ApplicationHookConfig
	logWriter io.Writer
}

func NewApplicationHook(config ApplicationHookConfig) logger.Hook {
	return &ApplicationHook{
		cfg: config,
		logWriter: &lumberjack.Logger{
			Filename:   config.Filename,
			MaxSize:    config.MaxSize,
			MaxBackups: config.MaxBackups,
			MaxAge:     config.MaxAge,
		},
	}
}

func (s *ApplicationHook) Levels() []logger.Level {
	return logger.AllLevels
}

func (s *ApplicationHook) Fire(entry *logger.Entry) error {
	b, err := s.cfg.Formatter.Format(entry)
	if err != nil {
		return err
	}
	_, err = s.logWriter.Write(b)
	return err
}
