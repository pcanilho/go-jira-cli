package interactive

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"strconv"
)

type OptType = int

const (
	CREATE OptType = iota + 1
	DUPLICATE
	READ
	UPDATE
	DELETE
)

var optMap = map[OptType]string{
	CREATE:    "+ Create",
	DUPLICATE: "| Duplicate",
	READ:      "<> Read",
	UPDATE:    "~ Update",
	DELETE:    "- Delete",
}

type Opt func(*commonCrudMenu)

func WithCreate() Opt {
	return func(menu *commonCrudMenu) {
		menu.crudItems = append(menu.crudItems, optMap[CREATE])
	}
}

func WithRead() Opt {
	return func(menu *commonCrudMenu) {
		menu.crudItems = append(menu.crudItems, optMap[READ])
	}
}

func WithUpdate() Opt {
	return func(menu *commonCrudMenu) {
		menu.crudItems = append(menu.crudItems, optMap[UPDATE])
	}
}

func WithDelete() Opt {
	return func(menu *commonCrudMenu) {
		menu.crudItems = append(menu.crudItems, optMap[DELETE])
	}
}

func WithDuplicate() Opt {
	return func(menu *commonCrudMenu) {
		menu.crudItems = append(menu.crudItems, optMap[DUPLICATE])
	}
}

type commonCrudMenu struct {
	promptui.Select
	previous, next Menu
	controller     internal.IssueController
	crudItems      []any
}

var commonCrudTemplate = &promptui.SelectTemplates{
	Label:    "-------- CRUD: {{ . }} --------",
	Active:   "→ {{ . }}",
	Inactive: "  {{ . }}",
	Selected: "{{ . }}",
}

func NewCommonCrudMenu(title string, previous Menu, opts ...Opt) *commonCrudMenu {
	instance := &commonCrudMenu{
		previous: previous,
		Select: promptui.Select{
			Label:        title,
			HideSelected: true,
			Templates:    commonCrudTemplate,
		},
	}

	if opts != nil {
		for _, opt := range opts {
			opt(instance)
		}
	}

	instance.crudItems = append([]any{PreviousMenu}, instance.crudItems)
	return instance
}

func (mcc *commonCrudMenu) Render() (int, string, error) {
	i, s, err := mcc.Run()
	var selectedType int
	if err != nil && i > 0 {
		switch s {
		case optMap[CREATE]:
			selectedType = CREATE
		case optMap[DUPLICATE]:
			selectedType = DUPLICATE
		case optMap[READ]:
			selectedType = READ
		case optMap[UPDATE]:
			selectedType = UPDATE
		case optMap[DELETE]:
			selectedType = DELETE
		}
	}
	return selectedType, s, err
}

func (mcc *commonCrudMenu) AttachController(controller internal.Controller) {
	mcc.controller = controller
}

func PromptFor(t OptType, item any) (bool, error) {
	var label string
	// cl := helpers.NewChangeLog()

	switch t {
	case DUPLICATE:
		label = fmt.Sprintf("Duplicate item [%v]?", item)
	case DELETE:
		label = fmt.Sprintf("Confirm item deletion [%v]?", item)
	}

	answer, err := (&promptui.Prompt{
		Label:       label,
		IsConfirm:   true,
		HideEntered: true,
	}).Run()

	if err != nil {
		return false, err
	}
	return strconv.ParseBool(answer)
}
