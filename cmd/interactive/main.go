package interactive

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"os"
)

type main struct {
	promptui.Select
	next       Menu
	controller internal.Controller
}

var mainItems = []string{"Users", "Issues", "Projects", "Exit"}

const (
	usersSelected int = iota
	issuesSelected
	projectsSelected
	exitSelected
)

var mainTemplate = &promptui.SelectTemplates{
	Label:    "-------- Main Menu --------",
	Active:   "→ {{ . | cyan }}",
	Inactive: "  {{ . | cyan }}",
	Selected: "  {{ . | red | cyan }}",
}

func NewMainMenu() Menu {
	return &main{
		Select: promptui.Select{
			HideSelected: true,
			Templates:    mainTemplate,
			Items:        mainItems,
		},
	}
}

func (m *main) AttachController(controller internal.Controller) {
	m.controller = controller
}

func (m *main) Render() (int, string, error) {
	if m.controller == nil {
		return -1, "", fmt.Errorf("the menu [controller] cannot be nil")
	}

	i, _, err := m.Run()
	if err != nil {
		os.Exit(0)
	}

	switch i {
	case usersSelected:
		m.next = NewUserCRUDMenu(m)
	case issuesSelected:
		il, err := m.controller.SearchIssues("", nil, 0)
		if err != nil {
			return -1, "", err
		}

		m.next = NewIssueListMenu(il, m)
	case projectsSelected:
		ps, err := m.controller.SearchProjects()
		if err != nil {
			return -1, "", err
		}

		m.next = NewIssueProjectMenu(ps, m)
	case exitSelected:
		os.Exit(0)
	}
	m.next.AttachController(m.controller)
	return m.next.Render()
}
