package interactive

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira-cli/internal"
)

type userPrompt struct {
	promptui.Prompt
	controller     internal.Controller
	next, previous Menu
}

func NewUserPrompt(previous Menu) Menu {
	return &userPrompt{
		previous: previous,
		Prompt: promptui.Prompt{
			HideEntered: true,
			Label:       "Get by username",
			AllowEdit:   true,
			IsVimMode:   true,
		},
	}
}

func (mup *userPrompt) AttachController(controller internal.Controller) {
	mup.controller = controller
}

func (mup *userPrompt) Render() (int, string, error) {
	if mup.controller == nil {
		return -1, "", fmt.Errorf("the menu [controller] cannot be nil")
	}

	answer, err := mup.Run()
	if err != nil {
		return mup.previous.Render()
	}

	u, err := mup.controller.GetUser(answer)
	if err != nil {
		fmt.Println(err)
		return mup.Render()
	}

	mup.next = NewUserMenu(*u, mup.previous)
	mup.next.AttachController(mup.controller)
	return mup.next.Render()
}
