package interactive

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"gitlab.com/pcanilho/go-jira"
	"gitlab.com/pcanilho/go-jira-cli/internal"
)

type user struct {
	promptui.Select
	next, previous Menu
	controller     internal.Controller
}

var userTemplate = &promptui.SelectTemplates{
	Label:    "-------- [{{ . }}] --------",
	Active:   `→ {{ .Name | cyan }} ({{ .Key | red }})`,
	Inactive: "  {{ .Name | cyan }} ({{ .Key | red }})",
	Selected: "→ {{ .Name | red | cyan }}",
	Details: `{{ if not .Name }} {{ . }} {{ else }}
----------- Details ------------
{{ "Active:" | faint }}	  {{ if .Active }}{{ .Active | green }}{{ else }}{{ .Active | red }}{{ end }}
{{ "Key:" | faint }}	  {{ .Key | underline }}
{{ "Name:" | faint }}	  {{ .Name }}
{{ "Display Name:" | faint }}	  {{ .DisplayName }}
{{ "Email Address:" | faint }}	  {{ .EmailAddress }}
{{ "TimeZone:" | faint }}	  {{ .TimeZone }}
{{ "Locale:" | faint }}  	  {{ .Locale }} {{ end }}`,
}

func NewUserMenu(u jira.User, previous Menu) Menu {
	return &user{
		previous: previous,
		Select: promptui.Select{
			HideSelected: true,
			Label:        u.Name,
			Items:        []any{u, PreviousMenu},
			Templates:    userTemplate,
		},
	}
}

func (mu *user) AttachController(controller internal.Controller) {
	mu.controller = controller
}

func (mu *user) Render() (int, string, error) {
	if mu.controller == nil {
		return -1, "", fmt.Errorf("the menu [controller] cannot be nil")
	}
	i, _, err := mu.Run()
	if err != nil {
		return mu.previous.Render()
	}

	switch i {
	case PreviousSelected + 1:
		mu.next = mu.previous
	default:
		// @TODO - UserDetailMenu
	}

	mu.next.AttachController(mu.controller)
	return mu.next.Render()
}
