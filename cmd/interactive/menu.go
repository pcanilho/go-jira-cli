package interactive

import "gitlab.com/pcanilho/go-jira-cli/internal"

const PreviousMenu = "↑ up..."
const (
	PreviousSelected int = iota
)

type Menu interface {
	Render() (int, string, error)
	AttachController(controller internal.Controller)
}
