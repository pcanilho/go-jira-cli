package helpers

import (
	"bytes"
	"gitlab.com/pcanilho/go-jira"
	"time"
)

var customFuncMap = map[string]any{
	"timeFormat": func(t jira.Time) string {
		return time.Time(t).Format(time.RFC850)
	},
	"projectComponentsToString": componentsSerialised,
}

func GenerateTemplateFuncMap(initialMap map[string]any) map[string]any {
	if initialMap == nil {
		return customFuncMap
	}
	for k, fn := range customFuncMap {
		initialMap[k] = fn
	}
	return initialMap
}

var componentsSerialised = func(cs []jira.ProjectComponent) string {
	var buf bytes.Buffer
	for i, c := range cs {
		buf.WriteString(c.Name)
		if i < (len(cs) - 2) {
			buf.WriteRune(',')
		}
	}
	return buf.String()
}
