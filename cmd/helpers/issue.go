package helpers

import (
	"gitlab.com/pcanilho/go-jira"
	"gopkg.in/yaml.v2"
)

func JiraIssueFieldsMap() map[string]any {
	out := make(map[string]any)
	b, _ := yaml.Marshal(jira.Issue{})
	_ = yaml.Unmarshal(b, &out)
	return out
}
