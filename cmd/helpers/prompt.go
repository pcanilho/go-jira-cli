package helpers

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"os"
	"strings"
)

const (
	promptMessage = "Do you wish to continue"
)

var (
	affirmativeOptions = []string{"y", "yes"}
)

func isAffirmative(result string) bool {
	result = strings.ToLower(strings.TrimSpace(result))
	for _, opt := range affirmativeOptions {
		if opt == result {
			return true
		}
	}
	return false
}

func PromptToContinue() (bool, error) {
	prompt := promptui.Prompt{
		Label:     promptMessage,
		IsConfirm: true,
		Default:   "No",
	}

	res, err := prompt.Run()
	if err != nil {
		return false, err
	}

	return isAffirmative(res), nil
}

func ExitWithAbort() {
	fmt.Println("Aborting...")
	os.Exit(0)
}

func ExitWithMessage(message string) {
	fmt.Println(message)
	os.Exit(0)
}
