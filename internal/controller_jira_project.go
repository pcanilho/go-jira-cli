package internal

import (
	"gitlab.com/pcanilho/go-jira"
	"log"
	"strings"
)

// https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/

func (j *jiraController) SearchProjects(pKeys ...string) ([]jira.Project, error) {
	var projectKeys string
	if pKeys != nil && len(pKeys) > 0 {
		projectKeys = strings.Join(pKeys, ",")
	}
	pList, _, err := j.client.Project.ListWithOptionsWithContext(j.ctx, &jira.GetQueryOptions{
		ProjectKeys: projectKeys,
		Expand:      "issueTypes,projectKeys",
	})
	if err != nil {
		return nil, err
	}

	var out []jira.Project
	for _, p := range *pList {
		log.Println(p.IssueTypes)
		out = append(out, jira.Project{
			Expand:          p.Expand,
			Self:            p.Self,
			ID:              p.ID,
			Key:             p.Key,
			Name:            p.Name,
			AvatarUrls:      p.AvatarUrls,
			ProjectCategory: p.ProjectCategory,
			IssueTypes:      p.IssueTypes,
		})
	}

	return out, nil
}
